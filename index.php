<?php
    require('animal.php');

    $sheep = new Animal("shaun"); 
    echo "Name: $sheep->name <br>"; // "shaun"
    echo "Legs: $sheep->legs <br>"; // 4
    echo "Cold Blooded: $sheep->cold_blooded <br><br>"; // "no"

    require('frog.php');

    $kodok = new Frog("buduk");
    echo "Name: $kodok->name <br>";  
    echo "Legs: $kodok->legs <br>";  
    echo "Cold Blooded: $sheep->cold_blooded <br>";  
    $kodok->jump();
    echo "<br><br>";  

    require('ape.php');

    $sungokong = new Ape("Kera sakti");
    echo "Name: $sungokong->name <br>";  
    echo "Legs: $sungokong->legs <br>";  
    echo "Cold Blooded: No <br>";  
    $sungokong->yell()."<br>";  


?>